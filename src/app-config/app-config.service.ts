import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppConfigService {
  constructor(private configService: ConfigService) {}

  get port(): number {
    return this.configService.get<number>('port');
  }

  get openExchangeRates(): string {
    return this.configService.get<string>('openExchangeRates.apiKey');
  }

  get openExchangeRatesBaseUrl(): string {
    return this.configService.get<string>('openExchangeRates.baseUrl');
  }
}
