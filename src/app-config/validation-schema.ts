import * as Joi from 'joi';

const schema = Joi.object({
  PORT: Joi.number().required(),
  OER_API_KEY: Joi.string().required(),
  OER_BASE_URL: Joi.string().required(),
});

export default schema;
