import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { Response } from 'express';
import { GeneralException, SystemException } from '../handlers';
import { ExceptionTypes } from '../../enums/exception.types';
import { ApiError } from '../../response/messages/api-errors';
import { ApiResponseError } from '../../types/api-response.interface';

@Catch(GeneralException, SystemException)
export class GeneralExceptionFilter implements ExceptionFilter {
  catch(exception: GeneralException | SystemException, host: ArgumentsHost) {
    const response = host.switchToHttp().getResponse<Response>();

    const status = exception.getStatus();
    const { code, message } = exception.getResponse() as ApiError;

    let type;

    switch (exception.constructor) {
      case SystemException:
        type = ExceptionTypes.System;
        break;

      default:
        type = ExceptionTypes.General;
        break;
    }

    const responseData: ApiResponseError = {
      error: true,
      type,
      code,
      message,
    };

    response.status(status).json(responseData);
  }
}
