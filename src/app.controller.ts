import { Controller, Get } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { AppService } from './app.service';
import { ConvertDto } from './dtos/convert.dto';
import { GetExRateResponse } from './types';

@Controller('/')
export class AppController {
  constructor(private readonly appService: AppService) {}

  // to get list of latest Exchange Rates.
  @MessagePattern('get_latest_data')
  async getLatestExchangeRates(): Promise<GetExRateResponse> {
    return this.appService.getLatestExchangeRates();
  }

  @MessagePattern('convert')
  async convert(convertDto: ConvertDto) {
    return this.appService.convert(convertDto);
  }
}
