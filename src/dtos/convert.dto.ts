import { IsNumber, IsString, Length } from 'class-validator';

export class ConvertDto {
  @IsString()
  @Length(3, 3)
  from: string;

  @IsString()
  @Length(3, 3)
  to: string;

  @IsNumber()
  amount: number;
}
