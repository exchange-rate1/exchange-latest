import { Logger } from '@nestjs/common';
import { HttpService, Injectable } from '@nestjs/common';
import { AppConfigService } from './app-config/app-config.service';
import { ConvertDto } from './dtos/convert.dto';
import { GeneralException } from './shared/exceptions/handlers';
import { ApiErrors } from './shared/response/messages';
import { GetExRateResponse } from './types';

@Injectable()
export class AppService {
  constructor(
    private readonly httpService: HttpService,
    private readonly appConfigService: AppConfigService,
  ) {}

  async getLatestExchangeRates(): Promise<GetExRateResponse> {
    try {
      const response = await this.httpService
        .get<GetExRateResponse>(
          `${this.appConfigService.openExchangeRatesBaseUrl}/latest.json?app_id=${this.appConfigService.openExchangeRates}`,
          {
            // transformation of json response from third-party API to Object
            transformResponse: (response): GetExRateResponse => {
              const parsedResponse = JSON.parse(response);
              return {
                timestamp: parsedResponse.timestamp,
                base: parsedResponse.base,
                rates: parsedResponse.rates,
              };
            },
          },
        )
        .toPromise();
      return response.data;
    } catch (error: any) {
      Logger.error(error);
      throw new GeneralException(ApiErrors.SystemErrorMessage);
    }
  }

  async convert({ amount, from, to }: ConvertDto) {
    const { rates } = await this.getLatestExchangeRates();

    if (!rates) {
      throw new GeneralException(ApiErrors.SystemErrorMessage);
    }

    if (!rates[from] || !rates[to]) {
      throw new GeneralException(ApiErrors.WrongCurrencyCode);
    }
    const convertedAmount = (rates[to] / rates[from]) * amount;

    return convertedAmount;
  }
}
